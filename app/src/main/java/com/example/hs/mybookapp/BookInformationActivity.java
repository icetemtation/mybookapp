package com.example.hs.mybookapp;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class BookInformationActivity extends Activity implements View.OnClickListener{

    // --- HTTP POST
    private static final String POST_id = "book_id";
    // --- Custom TAGs
    private static final String TAG_availability = "availability";
    private static final String TAG_LOG_RATING = "GetRatingAverage Task";
    // --- Constants
    private static final int FAILED = 0;
    private static final int SUCCESS = 1;
    private static final int NO_RESULT = 2;
    // --- Views
    private static TextView v_title;
    private static TextView v_ISBN10;
    private static TextView v_ISBN13;
    private static TextView v_rating;
    private static TextView v_location;
    private static TextView v_availability;
    private static TextView button_1;
    private static TextView button_2;
    private static TextView button_3;
    private static ImageView image_thumbnail;
    // --- Fragments
    private static FragmentManager f_manager;
    private static FragmentTransaction f_transaction;
    // --- Variables
    private static Bundle bundle;
    private static String averageRating;
    // --- Variables
    private static String totalReviews;
    private static String bookID;
    // --- Log
    private static final String LOG_TAG = "BookInfoActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get Bundle from previous activity
        bundle = getIntent().getExtras();
        bookID = bundle.getString(Constant.id);
        Log.d(LOG_TAG, "Received Book ID = " + bookID);
        setContentView(R.layout.activity_book_information);

        // Initialize view components
        v_title = (TextView) findViewById(R.id.book_title);
        v_ISBN10 = (TextView) findViewById(R.id.book_ISBN10);
        v_ISBN13 = (TextView) findViewById(R.id.book_ISBN13);
        v_rating = (TextView) findViewById(R.id.book_rating);
        v_location = (TextView) findViewById(R.id.book_location);
        v_availability = (TextView) findViewById(R.id.book_availability);
        button_1 = (TextView) findViewById(R.id.button1);
        button_2 = (TextView) findViewById(R.id.button2);
        button_3 = (TextView) findViewById(R.id.button3);
        image_thumbnail = (ImageView) findViewById(R.id.book_thumbnail);

        // Add click listener to buttons
        button_1.setOnClickListener(this);
        button_2.setOnClickListener(this);
        button_3.setOnClickListener(this);

        // Update book's info
        v_title.setText(bundle.getString(Constant.title));
        v_ISBN10.setText(bundle.getString(Constant.isbn10));
        v_ISBN13.setText(bundle.getString(Constant.isbn13));
        v_location.setText(bundle.getString(Constant.location));
        v_availability.setText("(" + bundle.getString(TAG_availability) + ")");
        v_availability.setTextColor(bundle.getString(TAG_availability).equals("In Stock") ?
                getResources().getColor(R.color.true_green) : getResources().getColor(R.color.false_red));

        // Initialize fragments
        BookInformationFragment f_bookInfo = new BookInformationFragment();
        f_bookInfo.setArguments(bundle);
        f_manager = getFragmentManager();
        f_transaction = f_manager.beginTransaction();
        f_transaction.add(R.id.scrollView, f_bookInfo);
        f_transaction.commit();

        // Re-set default view to "Reviews" tab
        if(bundle.getString(Constant.TAG_mode).equalsIgnoreCase(Constant.MODE_reviews)) {
            reviews();
        }

        // Load thumbnail
        new LoadImageTask(image_thumbnail).execute(bundle.getString(Constant.thumbnail));
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Update rating
        new GetAverageRatingTask().execute();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.button1 :
                details();
                break;

            case R.id.button2 :
                reviews();
                break;

            case R.id.button3 :
                others();
                break;
        }
    }

    private void others() {
        button_3.setBackgroundColor(getResources().getColor(R.color.selected_grey));
        button_1.setBackgroundColor(Color.TRANSPARENT);
        button_2.setBackgroundColor(Color.TRANSPARENT);
        replaceFragmentView(R.id.scrollView, new BookOtherFragment(), "f_bookOther");
    }

    private void reviews() {
        button_2.setBackgroundColor(getResources().getColor(R.color.selected_grey));
        button_1.setBackgroundColor(Color.TRANSPARENT);
        button_3.setBackgroundColor(Color.TRANSPARENT);
        replaceFragmentView(R.id.scrollView, new BookReviewFragment(), "f_bookReview");
    }

    private void details() {
        button_1.setBackgroundColor(getResources().getColor(R.color.selected_grey));
        button_2.setBackgroundColor(Color.TRANSPARENT);
        button_3.setBackgroundColor(Color.TRANSPARENT);
        replaceFragmentView(R.id.scrollView, new BookInformationFragment(), "BookInfo");
    }

    public void replaceFragmentView(int viewToBeReplaced , Fragment fragmentToReplace, String fragment_tag) {
        fragmentToReplace.setArguments(bundle);
        f_manager = getFragmentManager();
        f_transaction = f_manager.beginTransaction();
        f_transaction.replace(viewToBeReplaced, fragmentToReplace, fragment_tag);
        f_transaction.commit();
    }

    public class GetAverageRatingTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {

            JSONParser jParser = new JSONParser();

            try {
                // Sending book_id to server
                List<NameValuePair> kv = new ArrayList<>();
                kv.add(new BasicNameValuePair(POST_id, bundle.getString(Constant.id)));

                // Make HTTP POST request
                JSONObject json = jParser.makeHttpRequest(Constant.URL_getAverageRating, "POST", kv);

                // Getting result from server
                int status = json.getInt(Constant.status);

                if(status == SUCCESS) {
                    totalReviews = json.getJSONArray(Constant.Rating.rating).getJSONObject(0).getString(Constant.Rating.totalReviews);
                    averageRating = json.getJSONArray(Constant.Rating.rating).getJSONObject(0).getString(Constant.Rating.averageRating);
                    return true;
                } else if (status == NO_RESULT) {
                    return false;
                }

            } catch (Exception e) {
                Log.e(TAG_LOG_RATING, e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            if(aBoolean != null) {
                if (aBoolean) {
                    v_rating.setText(averageRating);
                    Toast.makeText(getApplicationContext(), "Average Rating loaded successfully.", Toast.LENGTH_SHORT).show();
                } else {
                    v_rating.setText("0.0");
                    Toast.makeText(getApplicationContext(), "No rating yet. Be the first one by leaving your review.", Toast.LENGTH_SHORT).show();
                }
            } else {
                v_rating.setText("Error Occurred");
            }
        }
    }
}
