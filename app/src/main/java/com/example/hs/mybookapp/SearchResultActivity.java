package com.example.hs.mybookapp;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListAdapter;

import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SearchResultActivity extends ListActivity {

    // --- Custom TAGs
    private static final String TAG_isbn = "isbn";
    private static final String TAG_availability = "availability";
    // --- Constants
    private static final int FAILED = 0;
    private static final int SUCCESS = 1;
    private static final int NO_RESULT = 2;
    // --- Bundle
    private static String searchBookInput, mode;
    // --- Local variables
    private String URL;
    private TextView btm_textView, top_title;
    private String btm_textView_text;
    private ProgressDialog pDialog;
    private JSONArray bookArray = null;
    private ArrayList<Book> bookList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);

        top_title = (TextView) findViewById(R.id.top_title);
        btm_textView = (TextView) findViewById(R.id.bottom_textView);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            searchBookInput = extras.getString(Constant.TAG_searchInput);
            Log.d("SearchActivity", "SEARCH BOOK INPUT ======== " + searchBookInput);
            mode = extras.getString(Constant.TAG_mode);
            Log.d("SearchActivity", "MODE ======== " + mode);

            if (mode.equalsIgnoreCase(Constant.MODE_details)) {
                top_title.setText("Search Result");
                URL = Constant.URL_getBookListByKeywordOrISBN;
            } else if (mode.equalsIgnoreCase(Constant.MODE_reviews)) {
                top_title.setText("Search Result");
                URL = Constant.URL_getBookListByKeywordOrISBN;
            } else if (mode.equalsIgnoreCase(Constant.MODE_shelfRecommendations)) {
                top_title.setText("Shelf Recommendations");
                URL = Constant.URL_getBookListByShelf;
            }
        }
        new SearchResultTask().execute();
    }



    private class SearchResultTask extends AsyncTask <Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            btm_textView.setText("Connecting to server ...");
            pDialog = new ProgressDialog(SearchResultActivity.this);
            pDialog.setMessage("Loading Search Result ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            bookList = new ArrayList<>();
            JSONParser jParser = new JSONParser();

            try {
                // Sending Keyword to server
                List<NameValuePair> kv = new ArrayList<>();

                // HTTP POST values
                if (mode.equalsIgnoreCase(Constant.MODE_shelfRecommendations)) {
                    kv.add(new BasicNameValuePair(Constant.POST.shelf, searchBookInput));
                } else {
                    kv.add(new BasicNameValuePair(Constant.POST.keyword, searchBookInput));
                }

                // Make HTTP POST request
                JSONObject json = jParser.makeHttpRequest(URL, "POST", kv);

                // Getting result from server
                int status = json.getInt(Constant.status);

                if (status == SUCCESS) {
                    bookArray = json.getJSONArray(Constant.books);

                    for (int i = 0; i < bookArray.length(); i++) {
                        JSONObject bookObject = bookArray.getJSONObject(i);
                        Book temp_book = new Book();
                        temp_book.setId(bookObject.getString(Constant.id));
                        temp_book.setIsbn10(bookObject.getString(Constant.isbn10));
                        temp_book.setIsbn13(bookObject.getString(Constant.isbn13));
                        temp_book.setTitle(bookObject.getString(Constant.title));
                        temp_book.setPrice(bookObject.getString(Constant.price));
                        temp_book.setGenre(bookObject.getString(Constant.genre));
                        temp_book.setAuthors(bookObject.getString(Constant.authors));
                        temp_book.setPublisher(bookObject.getString(Constant.publisher));
                        temp_book.setPublishedDate(bookObject.getString(Constant.publishedDate));
                        temp_book.setDescriptionShort(bookObject.getString(Constant.descShort));
                        temp_book.setDescriptionLong(bookObject.getString(Constant.descLong));
                        temp_book.setThumbnail(bookObject.getString(Constant.thumbnail));
                        temp_book.setPreview(bookObject.getString(Constant.preview));
                        temp_book.setStockQuantity(bookObject.getString(Constant.stockQuantity));
                        temp_book.setLocation(bookObject.getString(Constant.location));
                        bookList.add(temp_book);
                    }

                    btm_textView_text = "Matches " + json.getString(Constant.totalMatched) + " book(s).";
                } else if (status == NO_RESULT) {
                    btm_textView_text = "No matched result.\nPlease try with other keyword or ISBN.";
                }
                return true;
            } catch (NullPointerException e) {
                btm_textView_text = "Unable to connect to the server.\nPlease check your network connection.";
            }
            catch (Exception e) {
                btm_textView_text = "Server error.";
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            pDialog.dismiss();
            if(aBoolean) {
                ListAdapter adapter = new SearchResultAdapter(getApplicationContext(), R.layout.list_search_book_single, bookList);
                setListAdapter(adapter);
            }
            btm_textView.setText((btm_textView_text));
        }
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Toast.makeText(this, bookList.get(position).getId(), Toast.LENGTH_LONG).show();
        Intent i = new Intent(getBaseContext(), BookInformationActivity.class);
        i.putExtra(Constant.id, bookList.get(position).getId());
        i.putExtra(Constant.isbn10, bookList.get(position).getIsbn10());
        i.putExtra(Constant.isbn13, bookList.get(position).getIsbn13());
        i.putExtra(Constant.title, bookList.get(position).getTitle());
        i.putExtra(Constant.price, bookList.get(position).getPrice());
        i.putExtra(Constant.genre, bookList.get(position).getGenre());
        i.putExtra(Constant.authors, bookList.get(position).getAuthors());
        i.putExtra(Constant.publisher, bookList.get(position).getPublisher());
        i.putExtra(Constant.publishedDate, bookList.get(position).getPublishedDate());
        i.putExtra(Constant.descShort, bookList.get(position).getDescriptionShort());
        i.putExtra(Constant.descLong, bookList.get(position).getDescriptionLong());
        i.putExtra(Constant.thumbnail, bookList.get(position).getThumbnail());
        i.putExtra(Constant.preview, bookList.get(position).getPreview());
        i.putExtra(Constant.stockQuantity, bookList.get(position).getStockQuantity());
        i.putExtra(Constant.location, bookList.get(position).getLocation());
        i.putExtra(TAG_isbn, bookList.get(position).getIsbn());
        i.putExtra(TAG_availability, bookList.get(position).getAvailability());
        i.putExtra(Constant.TAG_mode, mode);
        startActivity(i);
    }
}
