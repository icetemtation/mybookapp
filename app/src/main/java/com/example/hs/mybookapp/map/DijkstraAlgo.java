package com.example.hs.mybookapp.map;

import android.graphics.Point;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DijkstraAlgo {

    private final List<Point> nodes;
    private final List<Edge> edges;
    private Set<Point> settledNodes;
    private Set<Point> unsettledNodes;
    private Map<Point, Point> predecessors;
    private Map<Point, Integer> distance;

    public DijkstraAlgo(Graph graph){
        this.nodes = new ArrayList<Point>(graph.getPoints());
        this.edges = new ArrayList<Edge>(graph.getEdges());
    }

    public void execute(Point source){
        settledNodes = new HashSet<Point>();
        unsettledNodes = new HashSet<Point>();
        distance = new HashMap<Point, Integer>();
        predecessors = new HashMap<Point, Point>();
        distance.put(source,0);

        unsettledNodes.add(source);

        while(unsettledNodes.size()>0){
            Point node = getMinimum(unsettledNodes);
            settledNodes.add(node);
            unsettledNodes.remove(node);
            findMinimalDistance(node);
        }

    }

    private void findMinimalDistance(Point node){
        List<Point> adjacentNodes = getNeighbors(node);
        for(Point target : adjacentNodes){
            if(getShortestDistance(target) > getShortestDistance(node) + getDistance(node,target)){
                distance.put(target, getShortestDistance(node)+getDistance(node,target));
                predecessors.put(target,node);
                unsettledNodes.add(target);
            }
        }
    }

    private int getDistance(Point node,Point target){
        for(Edge edge : edges){
            if(edge.getSource().equals(node) && edge.getDestination().equals(target)){
                return edge.getWeight();
            }
        }
        throw new RuntimeException("Should Not Happen");
    }

    private List<Point> getNeighbors(Point node){
        List<Point> neighbors = new ArrayList<Point>();
        for(Edge edge : edges){
            if(edge.getSource().equals(node) && !isSettled(edge.getDestination())){
                neighbors.add(edge.getDestination());
            }
        }
        return neighbors;
    }

    private boolean isSettled(Point point){
        return settledNodes.contains(point);
    }


    private Point getMinimum(Set<Point> points){
        Point minimum = null;
        for(Point point : points){
            if(minimum == null){
                minimum = point;
            }
            else{
                if(getShortestDistance(point) < getShortestDistance(minimum)){
                    minimum = point;
                }
            }
        }
        return minimum;
    }

    private int getShortestDistance(Point destination){
        Integer d = distance.get(destination);
        if(d==null)
            return Integer.MAX_VALUE;
        else
            return d;
    }

    //
    public LinkedList<Point> getPath(Point target){
        LinkedList<Point> path = new LinkedList<Point>();
        Point step = target;
        if(predecessors.get(step) == null){
            return null;
        }
        path.add(step);
        while(predecessors.get(step)!=null){
            step = predecessors.get(step);
            path.add(step);
        }

        Collections.reverse(path);
        return path;
    }

}
