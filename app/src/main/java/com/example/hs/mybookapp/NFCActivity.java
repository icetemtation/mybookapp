package com.example.hs.mybookapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;


public class NFCActivity extends ActionBarActivity {

    private static final String TAG = "NFC";
    private static final String TAG_dataFromNFC = "DATA_FROM_NFC";
    private static final String MIME_TEXT_PLAIN = "text/plain";

    private NfcAdapter mNfcAdapter;
    private String textFromNFCtag = null;
    private boolean NFCdetected = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nfc);

        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);

        if (mNfcAdapter == null) {
            // NFC not supported
            displayDialogBox(R.string.nfc_dialog_notSupported_title, R.string.nfc_dialog_notSupported_description);
        } else {
            if (!mNfcAdapter.isEnabled()) {
                // NFC Disabled
                displayDialogBox(R.string.nfc_dialog_disabled_title, R.string.nfc_dialog_disabled_description);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupForegroundDispatch(this, mNfcAdapter);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        // Vibrate for 500ms
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(500);

        getDataFromNFC(intent);
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopForegroundDispatch(this, mNfcAdapter);
        Log.d("NFC", "disableForegroundDispatch");
    }

    @Override
    public void finish() {

        if (NFCdetected) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra(TAG_dataFromNFC, textFromNFCtag);
            setResult(RESULT_OK, returnIntent);
            Log.d(TAG + "finish()", "NFCdetected = true");
        } else {
            Intent returnIntent = new Intent();
            setResult(RESULT_CANCELED, returnIntent);
            Log.d(TAG + "finish()", "NFCdetected = false");
        }
        super.finish();
    }

    private void displayDialogBox (int res_title, int res_description) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(res_title)
                .setMessage(res_description)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void getDataFromNFC(Intent intent) {
        Log.d(TAG, "process Intent");
        String action = intent.getAction();
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {

            String type = intent.getType();

            if (MIME_TEXT_PLAIN.equals(type)) {
                Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                Ndef ndef = Ndef.get(tag);

                // NDEF is not supported by this Tag.
                if (ndef == null) { textFromNFCtag = null; }

                NdefMessage ndefMessage = ndef.getCachedNdefMessage();
                NdefRecord[] records = ndefMessage.getRecords();

                for (NdefRecord ndefRecord : records) {
                    if (ndefRecord.getTnf() == NdefRecord.TNF_WELL_KNOWN && Arrays.equals(ndefRecord.getType(), NdefRecord.RTD_TEXT)) {
                        try {
                            textFromNFCtag = readText(ndefRecord);
                        } catch (UnsupportedEncodingException e) {
                            Log.e(TAG, "Unsupported Encoding", e);
                        }
                    }
                }

                if (textFromNFCtag != null) {
                    Log.d(TAG, "Received result = " + textFromNFCtag);
                    Toast.makeText(getBaseContext(), "Result is = " + textFromNFCtag, Toast.LENGTH_LONG).show();
                } else {
                    Log.d(TAG, "NULL RESULT");
                }
            } else {
                Log.d(TAG, "Wrong mime type: " + type);
            }
        }
        Log.d(TAG, "Finished getting String from NFC, textFromNFCtag = " + textFromNFCtag + "=====");
        NFCdetected = true;
    }

    private static void setupForegroundDispatch(final Activity activity, NfcAdapter adapter) {
        final Intent intent = new Intent(activity.getApplicationContext(), activity.getClass());
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        Log.d(TAG, "Setup ForegroundDispatch");
        final PendingIntent pendingIntent = PendingIntent.getActivity(activity.getApplicationContext(), 0, intent, 0);

        IntentFilter[] filters = new IntentFilter[1];
        String[][] techList = new String[][]{};

        // Notice that this is the same filter as in our manifest.
        filters[0] = new IntentFilter();
        filters[0].addAction(NfcAdapter.ACTION_NDEF_DISCOVERED);
        filters[0].addCategory(Intent.CATEGORY_DEFAULT);

        try {
            filters[0].addDataType(MIME_TEXT_PLAIN);
        } catch (IntentFilter.MalformedMimeTypeException e) {
            throw new RuntimeException("Check your mime type.");
        }

        adapter.enableForegroundDispatch(activity, pendingIntent, filters, techList);
        Log.d(TAG, "Done setup ForegroundDispatch");
    }

    private static void stopForegroundDispatch(final Activity activity, NfcAdapter adapter) {
        adapter.disableForegroundDispatch(activity);
        Log.d(TAG, "Done stop foreground dispatch");
    }

    private String readText(NdefRecord record) throws UnsupportedEncodingException {
        /*
         * See NFC forum specification for "Text Record Type Definition" at 3.2.1
         *
         * http://www.nfc-forum.org/specs/
         *
         * bit_7 defines encoding
         * bit_6 reserved for future use, must be 0
         * bit_5..0 length of IANA language code
         */
        Log.d(TAG, "READ TEXT");
        byte[] payload = record.getPayload();

        // Get the Text Encoding
        String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16";

        // Get the Language Code (127 = 7 bits which will removed all language prefix)
        int languageCodeLength = payload[0] & 0127;

        // String languageCode = new String(payload, 1, languageCodeLength, "US-ASCII");
        // e.g. "en"

        // Get the Text
        return new String(payload, languageCodeLength + 1, payload.length - languageCodeLength - 1, textEncoding);
    }

}