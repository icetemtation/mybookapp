package com.example.hs.mybookapp;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class BookReviewFragment extends Fragment {

    // --- Tags
    private static final String TAG_id = "id";
    // --- Constants
    private static final int SUCCESS = 1;
    private static final int NO_RESULT = 2;
    // --- Variables
    private static Bundle bundle;
    private static JSONArray reviewArray;
    private static ArrayList<Review> reviewList;
    private Activity activity;
    private static ListView list;
    private static Button button_addReview;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        bundle = this.getArguments();
        return inflater.inflate(R.layout.fragment_book_review, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        button_addReview = (Button) activity.findViewById(R.id.button_reviewAddReview);
        button_addReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_addReview = new Intent(activity, AddReviewActivity.class);
                intent_addReview.putExtra(TAG_id, bundle.getString(TAG_id));
                intent_addReview.putExtra(Constant.Review.title, bundle.getString(Constant.Review.title));
                startActivity(intent_addReview);
            }
        });
        list = (ListView) activity.findViewById(R.id.list_review);
    }

    @Override
    public void onResume() {
        super.onResume();
        new GetReviewTask().execute();
    }

    public class GetReviewTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            reviewList = new ArrayList<>();
            JSONParser jParser = new JSONParser();

            try {
                // Sending book_id to server
                List<NameValuePair> kv = new ArrayList<>();
                kv.add(new BasicNameValuePair(Constant.Review.bookID, bundle.getString(TAG_id)));

                // Make HTTP POST request
                JSONObject json = jParser.makeHttpRequest(Constant.URL_getReview, "POST", kv);

                // Getting result from server
                int status = json.getInt(Constant.status);

                if(status == SUCCESS) {
                    reviewArray = json.getJSONArray(Constant.Review.reviews);

                    for(int i = 0; i < reviewArray.length(); i++) {
                        JSONObject reviewObject = reviewArray.getJSONObject(i);
                        Review temp_review = new Review();

                        temp_review.setId(reviewObject.getString(Constant.Review.reviewId));
                        temp_review.setRating(reviewObject.getString(Constant.Review.rating));
                        temp_review.setComments(reviewObject.getString(Constant.Review.comments));
                        temp_review.setName(reviewObject.getString(Constant.Review.name));
                        temp_review.setDateTime(reviewObject.getString(Constant.Review.dateTime));

                        reviewList.add(temp_review);
                    }
                    return true;
                } else if (status == NO_RESULT) {
                    return false;
                }
            } catch (Exception e) {}
            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            if(aBoolean != null) {

                ListAdapter adapter = new GetReviewAdapter(activity.getApplicationContext(), R.layout.list_review_single, reviewList);
                list.setAdapter(adapter);

                if(aBoolean) {
                    Log.d("GetReview", "[SUCCESS] Reviews successfully loaded.");
                } else {
                    Log.d("GetReview", "[SUCCESS] There are no reviews.");
                }
            } else {
                Toast.makeText(activity, "Database / Network error occurred. Failed to load reviews", Toast.LENGTH_SHORT).show();
                Log.d("GetReview", "[FAILED] Database / Network error occurred. Failed to load reviews.");
            }
        }
    }
}
