package com.example.hs.mybookapp;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;


public class SearchReviewActivity extends ActionBarActivity {

    private static final int ACTIVITY_RESULT_QR_DRDROID = 0;

    EditText searchBook_editText_input;
    ImageButton searchBook_btn_clearField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_book);

        searchBook_editText_input = (EditText) findViewById(R.id.searchBook_editText_input);
        searchBook_btn_clearField = (ImageButton) findViewById(R.id.searchBook_btn_clearField);

        searchBook_editText_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if( searchBook_editText_input.getText().toString().length() > 0) {
                    searchBook_btn_clearField.setVisibility(View.VISIBLE);
                } else {
                    searchBook_btn_clearField.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });
    }

    /**
     * Button Action
     */
    public void clearField (View v) {
        searchBook_editText_input.setText("");
    }

    public void SearchBook (View v) {

        String searchBookInput = searchBook_editText_input.getText().toString();

        if( searchBookInput.trim().length() > 3 &&
                !searchBookInput.contains("/") &&
                !searchBookInput.contains("\\"))
        {
            // Set mode before launch intent
            Intent searchBook = new Intent(this, SearchResultActivity.class);
            searchBook.putExtra(Constant.TAG_mode, Constant.MODE_reviews);
            searchBook.putExtra(Constant.TAG_searchInput, searchBookInput);
            startActivity(searchBook);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.dialog_invalid_input_title)
                    .setMessage(R.string.dialog_invalid_input_description)
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    public void StartORDroid (View v) {
        try {
            Intent qrDroid = new Intent("la.droid.qr.scan");
            startActivityForResult(qrDroid, ACTIVITY_RESULT_QR_DRDROID);
        } catch (ActivityNotFoundException activity) {
            QRDroidNotInstalled();
        }
    }

    /**
     * Reads data scanned by user and returned by QR Droid
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if( ACTIVITY_RESULT_QR_DRDROID == requestCode && null != data && data.getExtras() != null ) {
            String result = data.getExtras().getString("la.droid.qr.result");
            EditText resultTxt = ( EditText ) findViewById(R.id.searchBook_editText_input);
            resultTxt.setText( result );
            resultTxt.setVisibility(View.VISIBLE);
        }
    }

    private void QRDroidNotInstalled () {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.QRDroid_not_found_dialog_title)
                .setMessage(R.string.QRDroid_not_found_dialog_description)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
        ((TextView)alert.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
    }
}
