package com.example.hs.mybookapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by HS on 28/01/2015.
 */
public class GetReviewAdapter extends ArrayAdapter<Review> {

    ArrayList<Review> reviewList;
    LayoutInflater layout_Inflater;
    ViewHolder holder;
    private int resource;

    public GetReviewAdapter (Context context, int resource, ArrayList<Review> objects) {
        super(context, resource, objects);

        layout_Inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.resource = resource;
        reviewList = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if(v == null) {
            holder = new ViewHolder();
            v = layout_Inflater.inflate(resource, null);

            holder.name = (TextView) v.findViewById(R.id.book_reviewName);
            holder.rating = (RatingBar) v.findViewById(R.id.book_reviewRating);
            holder.comment = (TextView) v.findViewById(R.id.book_reviewComment);
            holder.dateTime = (TextView) v.findViewById(R.id.book_reviewDateTime);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        holder.name.setText(reviewList.get(position).getName());
        holder.rating.setRating(Float.parseFloat(reviewList.get(position).getRating()));
        holder.dateTime.setText(reviewList.get(position).getDateTime());
        holder.comment.setText(reviewList.get(position).getComments());

        return v;
    }

    static class ViewHolder {
        public TextView name;
        public RatingBar rating;
        public TextView dateTime;
        public TextView comment;
    }
}
