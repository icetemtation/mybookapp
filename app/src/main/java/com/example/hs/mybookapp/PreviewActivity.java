package com.example.hs.mybookapp;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;


public class PreviewActivity extends ActionBarActivity {



    private static Bundle bundle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);

        bundle = getIntent().getExtras();

        WebView myWebView = (WebView) findViewById(R.id.webview);
        myWebView.loadUrl(bundle.getString(Constant.Preview.url));

        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

    }



}
