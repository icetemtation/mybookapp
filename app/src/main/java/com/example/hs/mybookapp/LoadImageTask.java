package com.example.hs.mybookapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.InputStream;

public class LoadImageTask extends AsyncTask<String, Void, Bitmap> {

    ImageView imgView;

    public LoadImageTask(ImageView imgView) {
        this.imgView = imgView;
    }

    @Override
    protected Bitmap doInBackground(String... url) {
        String url_thumbnail = url[0];
        Bitmap thumbnail = null;
        Log.d("URL", url_thumbnail);
        try {
            InputStream in = new java.net.URL(url_thumbnail).openStream();
            thumbnail = BitmapFactory.decodeStream(in);
            return thumbnail;
        } catch (Exception e) {
            Log.e("Thumbnail", "Failed to load " + url_thumbnail);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap thumbnail) {
        if(thumbnail != null) {
            imgView.setImageBitmap(thumbnail);
        } else {
            imgView.setImageResource(R.drawable.thumb_noimage);
        }
    }
}
