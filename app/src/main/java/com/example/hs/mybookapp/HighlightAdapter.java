package com.example.hs.mybookapp;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class HighlightAdapter extends ArrayAdapter<Book> {

    ArrayList<Book> bookList;
    LayoutInflater layout_Inflater;
    ViewHolder holder;
    private int resource;


    public HighlightAdapter (Context context, int resource, ArrayList<Book> objects) {
        super(context, resource, objects);

        layout_Inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.resource = resource;
        bookList = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if(v == null) {
            holder = new ViewHolder();
            v = layout_Inflater.inflate(resource, null);

            holder.imageView = (ImageView) v.findViewById(R.id.highlight_thumbnail);
            holder.title = (TextView) v.findViewById(R.id.highlight_title);
            holder.bookId = (TextView) v.findViewById(R.id.highlight_bookID);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        holder.imageView.setImageResource(R.drawable.thumb_loading);
        new LoadImageTask(holder.imageView).execute(bookList.get(position).getThumbnail());

        holder.title.setText(bookList.get(position).getTitle());
        holder.title.setEllipsize(TextUtils.TruncateAt.END);
        holder.title.setMaxLines(2);
        holder.bookId.setText(bookList.get(position).getId());

        return v;
    }

    static class ViewHolder {
        public ImageView imageView;
        public TextView title;
        public TextView bookId;
    }
}