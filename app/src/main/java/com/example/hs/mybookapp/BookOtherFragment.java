package com.example.hs.mybookapp;

import android.app.Activity;
import android.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

public class BookOtherFragment extends Fragment {

    // --- Constants
    private static final String NOT_AVAILABLE = "0";

    private static Bundle bundle;
    private Activity activity;
    private static Button button_navigate;
    private static Button button_preview;
    private double screen_height, screen_width;
    private static String url;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        bundle = this.getArguments();
        return inflater.inflate(R.layout.fragment_book_other, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initScreenSize();

        button_navigate = (Button) activity.findViewById(R.id.button_otherButton1);
        button_navigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_map = new Intent(activity, NavigationActivity.class);
                intent_map.putExtra(Constant.location, bundle.getString(Constant.location));
                startActivity(intent_map);
            }
        });

        button_preview = (Button) activity.findViewById(R.id.button_otherButton2);
        if (bundle.getString(Constant.preview).equals(NOT_AVAILABLE)) {
            button_preview.setEnabled(false);
            button_preview.setText("Book Preview\n(Not Available)");
        } else {
            Log.d("PREVIEW", "URL is === " + bundle.getString(Constant.preview));
            // TODO dynamic links
            // Sample http://mybookapp.comuf.com/getPreview.html?isbn=0738531367
            // url = "http://mybookapp.comuf.com/getPreview.php?isbn=0738531367" + "&width=" + screen_width + "&height=" + screen_height;
            url = bundle.getString(Constant.preview) + "&width=" + screen_width + "&height=" + screen_height ;

            button_preview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("BookPreview === FINAL ADDRESS ===", url);
                    Toast.makeText(activity, "Width = " + screen_width + "\nHeight = " + screen_height, Toast.LENGTH_LONG).show();
                    Intent intent_preview = new Intent(activity, PreviewActivity.class);
                    intent_preview.putExtra(Constant.Preview.url, url);
                    startActivity(intent_preview);
                }
            });
        }
    }

    private void initScreenSize() {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        this.screen_height = (displayMetrics.heightPixels / displayMetrics.density) * 0.95;
        this.screen_width = (displayMetrics.widthPixels / displayMetrics.density) * 0.95;
    }
}
