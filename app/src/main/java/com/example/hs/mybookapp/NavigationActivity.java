package com.example.hs.mybookapp;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;


public class NavigationActivity extends ActionBarActivity implements View.OnClickListener {

    // --- TAG
    private static final String TAG_location = "location";
    private static final String TAG_sourceShelf = "start";
    private static final String TAG_destinationShelf = "destination";
    private static final String TAG_dataFromNFC = "DATA_FROM_NFC";
    private static final int CODE_useQRcode = 0;
    private static final int CODE_useNFC = 1;

    private static Spinner spinner_source;
    private static Spinner spinner_destination;
    private static ArrayAdapter<CharSequence> adapter;
    private static LinearLayout linear_destination;
    private static Bundle bundle;
    private static Button source_useQRcode, source_useNFC;
    private static Button button_navigate;
    private static String sourceShelf;
    private static String destinationShelf;
    private static boolean destination_flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);

        // Initialize views
        linear_destination = (LinearLayout) findViewById(R.id.navigation_destination);
        spinner_source = (Spinner) findViewById(R.id.spinner_shelf_source);
        spinner_destination = (Spinner) findViewById(R.id.spinner_shelf_destination);
        initSpinner(spinner_source);

        // Get Destination
        bundle = getIntent().getExtras();
        if(bundle != null) {
            destinationShelf = bundle.getString(TAG_location);
            destination_flag = false;
        } else {
            linear_destination.setVisibility(View.VISIBLE);
            initSpinner(spinner_destination);
            destination_flag = true;
        }

        // Initialize Button
        source_useQRcode = (Button) findViewById(R.id.source_useQRCode);
        source_useQRcode.setOnClickListener(this);
        source_useNFC = (Button) findViewById(R.id.source_useNFC);
        source_useNFC.setOnClickListener(this);
        button_navigate = (Button) findViewById(R.id.button);
        button_navigate.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.source_useQRCode :
                try {
                    Intent qrDroid = new Intent("la.droid.qr.scan");
                    startActivityForResult(qrDroid, CODE_useQRcode);
                } catch (ActivityNotFoundException activity) {
                    displayAlertDialog(R.string.QRDroid_not_found_dialog_title, R.string.QRDroid_not_found_dialog_description);
                }
                break;

            case R.id.source_useNFC :
                Intent i = new Intent(this, NFCActivity.class);
                startActivityForResult(i, CODE_useNFC);
                break;

            case R.id.button :
                navigate();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CODE_useQRcode && null!=data && data.getExtras()!= null) {
            String dataFromQRcode = data.getExtras().getString("la.droid.qr.result");
            Log.d("==NavigationActivity==", "Data received is (FROM QR Code) == " + dataFromQRcode);
            verifyDataAndNavigate(dataFromQRcode);
        }

        if (requestCode == CODE_useNFC) {
            if(resultCode == RESULT_OK){
                String dataFromNFC = data.getStringExtra(TAG_dataFromNFC);
                Log.d("==NavigationActivity==", "Data received is (FROM NFC) == " + dataFromNFC);
                verifyDataAndNavigate(dataFromNFC);
            }
            if (resultCode == RESULT_CANCELED) {
                //Write your code if there's no result
                Toast.makeText(this, "Cancelled by user", Toast.LENGTH_LONG).show();
                Log.d("==NavigationActivity==", "Cancelled by user");
            }
        }
    }

    private void verifyDataAndNavigate(String dataFromNFC) {
        dataFromNFC = dataFromNFC.toUpperCase();

        if (dataFromNFC.contains("MYBOOKAPP")) {
            if (dataFromNFC.contains("SHELF.A")) {
                setSourceSpinner("A");
            } else if (dataFromNFC.contains("SHELF.B")) {
                setSourceSpinner("B");
            } else if (dataFromNFC.contains("SHELF.C")) {
                setSourceSpinner("C");
            } else if (dataFromNFC.contains("SHELF.D")) {
                setSourceSpinner("D");
            } else if (dataFromNFC.contains("SHELF.E")) {
                setSourceSpinner("E");
            } else if (dataFromNFC.contains("SHELF.F")) {
                setSourceSpinner("F");
            } else if (dataFromNFC.contains("SHELF.G")) {
                setSourceSpinner("G");
            } else if (dataFromNFC.contains("SHELF.H")) {
                setSourceSpinner("H");
            } else if (dataFromNFC.contains("SHELF.I")) {
                setSourceSpinner("I");
            } else if (dataFromNFC.contains("SHELF.J")) {
                setSourceSpinner("J");
            }

            // Navigate
            navigate();
        } else {
            // Unsupported data received
            displayAlertDialog(R.string.navigation_dialog_unknown_data_title, R.string.navigation_dialog_unknown_data_description);
        }
    }

    private void setSourceSpinner(String spinnerValue) {
        int spinnerPosition = adapter.getPosition(spinnerValue);
        spinner_source.setSelection(spinnerPosition);
    }

    private void navigate() {
        // Check if requires destination input
        if (destination_flag) {
            destinationShelf = spinner_destination.getSelectedItem().toString();
        }

        // Launch map if source and destination shelf are not the same
        if (validateSourceDestination()) {
            launchMapIntent();
        } else {
            displayAlertDialog(R.string.navigation_dialog_source_destination_title, R.string.navigation_dialog_source_destination_description);
        }
    }

    /*
        To validate shelf's source and destination point
        - return true if source and destination point are different
     */
    private boolean validateSourceDestination() {
        sourceShelf = spinner_source.getSelectedItem().toString();
        return sourceShelf.charAt(0) != destinationShelf.charAt(0);
    }

    private void launchMapIntent() {
        Intent intent_map = new Intent(getApplicationContext(), MapActivity.class);
        intent_map.putExtra(TAG_sourceShelf, sourceShelf);
        intent_map.putExtra(TAG_destinationShelf, destinationShelf);
        startActivity(intent_map);
    }

    private void displayAlertDialog(int title, int description) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title)
                .setMessage(description)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void initSpinner(Spinner spinner) {
        adapter = ArrayAdapter.createFromResource(this,
                R.array.spinner_shelf, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

}
