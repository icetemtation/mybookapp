package com.example.hs.mybookapp;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.ImageView;

import java.util.LinkedList;

/**
 * Created by HS on 02/02/2015.
 */
public class DrawMap extends ImageView {

    private double screen_height, screen_width;
    private static Paint paint = new Paint();
    private static Paint point = new Paint();
    private LinkedList<Point> path = new LinkedList<>();

    public DrawMap(Context context) {
        super(context);
    }

    public DrawMap(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DrawMap(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        initScreenSize();

        paint.setColor(Color.CYAN);
        paint.setStrokeWidth(10);
        for(int i = 0; i < path.size() - 1; i++) {

            // Draw patch
            canvas.drawLine(responsivePositionX(path.get(i).x), responsivePositionY(path.get(i).y),
                    responsivePositionX(path.get(i + 1).x), responsivePositionY(path.get(i + 1).y), paint);

            // Smoothen the edges by adding a circle
            if(i == 0) {
                // Starting point
                point.setColor(Color.GREEN);
                canvas.drawCircle(responsivePositionX(path.get(i).x), responsivePositionY(path.get(i).y), 10, point);
            } else if (i == path.size() - 2) {
                // Edges
                point.setColor(Color.CYAN);
                canvas.drawCircle(responsivePositionX(path.get(i).x), responsivePositionY(path.get(i).y), 5, point);
                // Ending point
                point.setColor(Color.RED);
                canvas.drawCircle(responsivePositionX(path.get(i + 1).x), responsivePositionY(path.get(i + 1).y), 10, point);
            } else {
                // Edges
                point.setColor(Color.CYAN);
                canvas.drawCircle(responsivePositionX(path.get(i).x), responsivePositionY(path.get(i).y), 5, point);
            }
        }
    }

    public void pathToDraw(LinkedList<Point> path,Point endPoint){
        this.path = path;
        path.add(endPoint);
    }

    private void initScreenSize() {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        this.screen_height = displayMetrics.heightPixels;
        this.screen_width = displayMetrics.widthPixels;

        Log.d("=== Screen Size ===", "Height = " + screen_height);
        Log.d("=== Screen Size ===", "Width = " + screen_width);
    }

    // This method will return a responsive position-X depends on mobile screen sizes
    private float responsivePositionX(float position) {
        return (float) (position / 1280 * screen_height);
    }

    // This method will return a responsive position-Y depends on mobile screen sizes
    private float responsivePositionY(float position) {
        return (float) (position / 720 * screen_width);
    }
}
