package com.example.hs.mybookapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class SearchResultAdapter extends ArrayAdapter<Book> {

    ArrayList<Book> bookList;
    LayoutInflater layout_Inflater;
    ViewHolder holder;
    private int resource;


    public SearchResultAdapter (Context context, int resource, ArrayList<Book> objects) {
        super(context, resource, objects);

        layout_Inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.resource = resource;
        bookList = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if(v == null) {
            holder = new ViewHolder();
            v = layout_Inflater.inflate(resource, null);

            holder.imageView = (ImageView) v.findViewById(R.id.thumbnail);
            holder.title = (TextView) v.findViewById(R.id.title);
            holder.author = (TextView) v.findViewById(R.id.author);
            holder.availability = (TextView) v.findViewById(R.id.availability);
            holder.bookId = (TextView) v.findViewById(R.id.bookID);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        holder.imageView.setImageResource(R.drawable.thumb_loading);
        new LoadImageTask(holder.imageView).execute(bookList.get(position).getThumbnail());

        holder.title.setText(bookList.get(position).getTitle());
        holder.author.setText(bookList.get(position).getAuthors());
        holder.availability.setText(bookList.get(position).getAvailability());
        holder.bookId.setText(bookList.get(position).getId());

        return v;
    }

    static class ViewHolder {
        public ImageView imageView;
        public TextView title;
        public TextView author;
        public TextView availability;
        public TextView bookId;
    }
}
