package com.example.hs.mybookapp;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class BookInformationFragment extends Fragment {

    // --- Views
    private static TextView v_price;
    private static TextView v_genre;
    private static TextView v_author;
    private static TextView v_publishedDate;
    private static TextView v_publisher;
    private static TextView v_description;
    private View v_bookInformation;
    private static Bundle bundle;
    // --- Log
    private static final String LOG_TAG = "BookInfoFragment";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Get Book ID from activity
        Log.d(LOG_TAG, "Get bookID from activity");
        bundle = this.getArguments();
        v_bookInformation = inflater.inflate(R.layout.fragment_book_information, null);
        return v_bookInformation;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Log.d(LOG_TAG, "Initializing views");
        // Initialize views
        v_price = (TextView) v_bookInformation.findViewById(R.id.book_price);
        v_genre = (TextView) v_bookInformation.findViewById(R.id.book_genre);
        v_author = (TextView) v_bookInformation.findViewById(R.id.book_author);
        v_publishedDate = (TextView) v_bookInformation.findViewById(R.id.book_publishedDate);
        v_publisher = (TextView) v_bookInformation.findViewById(R.id.book_publisher);
        v_description = (TextView) v_bookInformation.findViewById(R.id.book_description);

        // Update book's info
        v_price.setText(bundle.getString(Constant.price));
        v_genre.setText(bundle.getString(Constant.genre));
        v_author.setText(bundle.getString(Constant.authors));
        v_publishedDate.setText(bundle.getString(Constant.publishedDate));
        v_publisher.setText(bundle.getString(Constant.publisher));
        v_description.setText(bundle.getString(Constant.descLong));

        Log.d(LOG_TAG, "DONE updating book's info.");
    }
}
