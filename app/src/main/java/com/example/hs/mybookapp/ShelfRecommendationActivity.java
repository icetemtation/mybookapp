package com.example.hs.mybookapp;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;


public class ShelfRecommendationActivity extends ActionBarActivity implements View.OnClickListener {

    // --- TAG
    private static final String TAG_dataFromNFC = "DATA_FROM_NFC";
    private static final int CODE_useQRcode = 0;
    private static final int CODE_useNFC = 1;

    private static Spinner spinner;
    private static ArrayAdapter<CharSequence> adapter;
    private static Button button_useQRcode, button_useNFC, button_recommendation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shelf_highlight);

        // Initialize spinner
        spinner = (Spinner) findViewById(R.id.spinner_shelf);
        initSpinner(spinner);

        button_useQRcode = (Button) findViewById(R.id.recommendation_useQRCode);
        button_useQRcode.setOnClickListener(this);
        button_useNFC = (Button) findViewById(R.id.recommendation_useNFC);
        button_useNFC.setOnClickListener(this);
        button_recommendation = (Button) findViewById(R.id.searchRecommendation);
        button_recommendation.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.recommendation_useQRCode :
                try {
                    Intent qrDroid = new Intent("la.droid.qr.scan");
                    startActivityForResult(qrDroid, CODE_useQRcode);
                } catch (ActivityNotFoundException activity) {
                    displayAlertDialog(R.string.QRDroid_not_found_dialog_title, R.string.QRDroid_not_found_dialog_description);
                }
                break;

            case R.id.recommendation_useNFC :
                Intent i = new Intent(this, NFCActivity.class);
                startActivityForResult(i, CODE_useNFC);
                break;

            case R.id.searchRecommendation :
                searchRecommendation();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CODE_useQRcode && null!=data && data.getExtras()!= null) {
            String dataFromQRcode = data.getExtras().getString("la.droid.qr.result");
            Log.d("==NavigationActivity==", "Data received is (FROM QR Code) == " + dataFromQRcode);
            verifyDataAndSearchRecommendation(dataFromQRcode);
        }

        if (requestCode == CODE_useNFC) {
            if(resultCode == RESULT_OK){
                String dataFromNFC = data.getStringExtra(TAG_dataFromNFC);
                Log.d("==NavigationActivity==", "Data received is (FROM NFC) == " + dataFromNFC);
                verifyDataAndSearchRecommendation(dataFromNFC);
            }
            if (resultCode == RESULT_CANCELED) {
                //Write your code if there's no result
                Toast.makeText(this, "Cancelled by user", Toast.LENGTH_LONG).show();
                Log.d("==NavigationActivity==", "Cancelled by user");
            }
        }
    }

    private void verifyDataAndSearchRecommendation(String dataFromNFC) {
        dataFromNFC = dataFromNFC.toUpperCase();

        if (dataFromNFC.contains("MYBOOKAPP")) {
            if (dataFromNFC.contains("SHELF.A")) {
                setSpinner("A");
            } else if (dataFromNFC.contains("SHELF.B")) {
                setSpinner("B");
            } else if (dataFromNFC.contains("SHELF.C")) {
                setSpinner("C");
            } else if (dataFromNFC.contains("SHELF.D")) {
                setSpinner("D");
            } else if (dataFromNFC.contains("SHELF.E")) {
                setSpinner("E");
            } else if (dataFromNFC.contains("SHELF.F")) {
                setSpinner("F");
            } else if (dataFromNFC.contains("SHELF.G")) {
                setSpinner("G");
            } else if (dataFromNFC.contains("SHELF.H")) {
                setSpinner("H");
            } else if (dataFromNFC.contains("SHELF.I")) {
                setSpinner("I");
            } else if (dataFromNFC.contains("SHELF.J")) {
                setSpinner("J");
            }

            // Recommendation
            searchRecommendation();
        } else {
            // Unsupported data received
            displayAlertDialog(R.string.navigation_dialog_unknown_data_title, R.string.navigation_dialog_unknown_data_description);
        }
    }

    private void searchRecommendation() {
        // Set mode before launch intent
        Intent searchBook = new Intent(this, SearchResultActivity.class);
        searchBook.putExtra(Constant.TAG_mode, Constant.MODE_shelfRecommendations);
        searchBook.putExtra(Constant.TAG_searchInput, spinner.getSelectedItem().toString());
        Log.d("Shelf Highlight", "Selected Shelf : ========= " + spinner.getSelectedItem().toString());
        startActivity(searchBook);
    }

    private void setSpinner(String spinnerValue) {
        int spinnerPosition = adapter.getPosition(spinnerValue);
        spinner.setSelection(spinnerPosition);
    }

    private void initSpinner(Spinner spinner) {
        adapter = ArrayAdapter.createFromResource(this,
                R.array.spinner_shelf, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    private void displayAlertDialog(int title, int description) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title)
                .setMessage(description)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
