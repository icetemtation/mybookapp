package com.example.hs.mybookapp;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent i = new Intent(getBaseContext(), MainMenuActivity.class);
        startActivity(i);

        // TODO : Landing Loading Page
        finish();
    }
}
