package com.example.hs.mybookapp.map;

import android.graphics.Point;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Floor {
    private List<Point> nodes;
    private List<Edge> edges;
    private LinkedList<Point> path;

    public Floor(){
        nodes = new ArrayList<Point>();
        edges = new ArrayList<Edge>();

    }

    public List<Point> getNodes() { return nodes; }

    public void setNodes(List<Point> nodes) { this.nodes = nodes; }

    public LinkedList<Point> getPath() { return path; }

    public void addNode(Point point){
        nodes.add(point);
    }

    public void addLane(String laneID, int sourceLoc, int destLoc){
        int duration = calculateDuration(nodes.get(sourceLoc),nodes.get(destLoc));
        Edge lane = new Edge(laneID,nodes.get(sourceLoc), nodes.get(destLoc), duration);
        Edge reverseLane = new Edge(laneID+"Reverse", nodes.get(destLoc), nodes.get(sourceLoc), duration);
        edges.add(lane);
        edges.add(reverseLane);
    }

    private int calculateDuration(Point source,Point dest){
        return (int)Math.sqrt(Math.pow(source.x - dest.x, 2) + Math.pow(source.y - dest.y,2));
    }

    public LinkedList<Point> getRoute(int startPoint, int endPoint){
        LinkedList<Point> path;
        Graph graph = new Graph(nodes,edges);
        DijkstraAlgo algo = new DijkstraAlgo(graph);
        algo.execute(nodes.get(startPoint));
        path = algo.getPath(nodes.get(endPoint));
        return path;
    }
}
