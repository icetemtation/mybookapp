package com.example.hs.mybookapp.map;

/**
 * Created by HS on 26/02/2015.
 */
public class Shelf {
    public static final int A_X = 170;
    public static final int A_Y = 355;

    public static final int B_X = 170;
    public static final int B_Y = 650;

    public static final int C_X = 170;
    public static final int C_Y = 955;

    public static final int D_X = 200;
    public static final int D_Y = 650;

    public static final int E_X = 200;
    public static final int E_Y = 955;

    public static final int F_X = 525;
    public static final int F_Y = 650;

    public static final int G_X = 525;
    public static final int G_Y = 955;

    public static final int H_X = 555;
    public static final int H_Y = 355;

    public static final int I_X = 555;
    public static final int I_Y = 650;

    public static final int J_X = 555;
    public static final int J_Y = 955;

    public static int getX(char shelf) {
        int coordinate = 0;

        switch (shelf) {
            case 'A'    :   coordinate = A_X; break;
            case 'B'    :   coordinate = B_X; break;
            case 'C'    :   coordinate = C_X; break;
            case 'D'    :   coordinate = D_X; break;
            case 'E'    :   coordinate = E_X; break;
            case 'F'    :   coordinate = F_X; break;
            case 'G'    :   coordinate = G_X; break;
            case 'H'    :   coordinate = H_X; break;
            case 'I'    :   coordinate = I_X; break;
            case 'J'    :   coordinate = J_X; break;
            default     :   break;
        }
        return coordinate;
    }

    public static int getY(char shelf) {
        int coordinate = 0;

        switch (shelf) {
            case 'A'    :   coordinate = A_Y; break;
            case 'B'    :   coordinate = B_Y; break;
            case 'C'    :   coordinate = C_Y; break;
            case 'D'    :   coordinate = D_Y; break;
            case 'E'    :   coordinate = E_Y; break;
            case 'F'    :   coordinate = F_Y; break;
            case 'G'    :   coordinate = G_Y; break;
            case 'H'    :   coordinate = H_Y; break;
            case 'I'    :   coordinate = I_Y; break;
            case 'J'    :   coordinate = J_Y; break;
            default     :   break;
        }
        return coordinate;
    }
}
