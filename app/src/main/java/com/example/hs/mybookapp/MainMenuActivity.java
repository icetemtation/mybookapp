package com.example.hs.mybookapp;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainMenuActivity extends ActionBarActivity implements View.OnClickListener {

    ImageButton imageButton_searchBook, imageButton_review, imageButton_navigation,
                imageButton_information, imageButton_bookRecommend, imageButton_shelfHighlight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        isInternetOn();

        imageButton_searchBook = (ImageButton) findViewById(R.id.mainmenu_imageButton_searchBook);
        imageButton_review = (ImageButton) findViewById(R.id.mainmenu_imageButton_review);
        imageButton_navigation = (ImageButton) findViewById(R.id.mainmenu_imageButton_navigation);
        imageButton_information = (ImageButton) findViewById(R.id.mainmenu_imageButton_information);
        imageButton_bookRecommend = (ImageButton) findViewById(R.id.mainmenu_imageButton_bookRecommend);
        imageButton_shelfHighlight = (ImageButton) findViewById(R.id.mainmenu_imageButton_shelfRecommendation);

        imageButton_searchBook.setOnClickListener(this);
        imageButton_review.setOnClickListener(this);
        imageButton_navigation.setOnClickListener(this);
        imageButton_information.setOnClickListener(this);
        imageButton_bookRecommend.setOnClickListener(this);
        imageButton_shelfHighlight.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.mainmenu_imageButton_searchBook :
                Intent intent_searchBook = new Intent(this, SearchBookActivity.class);
                startActivity(intent_searchBook);
                break;

            case R.id.mainmenu_imageButton_review :
                Intent intent_review = new Intent(this, SearchReviewActivity.class);
                startActivity(intent_review);
                break;

            case R.id.mainmenu_imageButton_navigation :
                Intent intent_navigation = new Intent(this, NavigationActivity.class);
                startActivity(intent_navigation);
                break;

            case R.id.mainmenu_imageButton_information :
                Intent intent_bookInfo = new Intent(this, SearchBookActivity.class);
                startActivity(intent_bookInfo);
                break;

            case R.id.mainmenu_imageButton_bookRecommend :
                Intent intent_bookHighlight = new Intent(this, BookHighlightActivity.class);
                startActivity(intent_bookHighlight);
                break;

            case R.id.mainmenu_imageButton_shelfRecommendation:
                Intent intent_shelfHighlight = new Intent(this, ShelfRecommendationActivity.class);
                startActivity(intent_shelfHighlight);
                break;
        }
    }

    public final boolean isInternetOn() {

        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        // Check for network connections
        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {

            // if connected with internet

            Toast.makeText(this, " Connected ", Toast.LENGTH_LONG).show();
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {

            Toast.makeText(this, " NO INTERNET ACCESS ", Toast.LENGTH_LONG).show();
            return false;
        }
        return false;
    }

}
