package com.example.hs.mybookapp;

/**
 * Created by HS on 22/01/2015.
 */
public class Book {

    private String id;
    private String isbn10;
    private String isbn13;
    private String title;
    private String price;
    private String genre;
    private String authors;
    private String publisher;
    private String publishedDate;
    private String descriptionShort;
    private String descriptionLong;
    private String thumbnail;
    private String preview;
    private String stockQuantity;
    private String location;

    // Custom
    private String isbn;
    private String availability;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIsbn10() {
        return isbn10;
    }

    public void setIsbn10(String isbn10) {
        this.isbn10 = isbn10;
    }

    public String getIsbn13() {
        return isbn13;
    }

    public void setIsbn13(String isbn13) {
        this.isbn13 = isbn13;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(String publishedDate) {
        this.publishedDate = publishedDate;
    }

    public String getDescriptionShort() {
        return descriptionShort;
    }

    public void setDescriptionShort(String descriptionShort) {
        this.descriptionShort = descriptionShort;
    }

    public String getDescriptionLong() {
        return descriptionLong;
    }

    public void setDescriptionLong(String descriptionLong) {
        this.descriptionLong = descriptionLong;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    public String getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(String stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getIsbn() {
        if(isbn10 == "null") {
            isbn = isbn13;
        } else if(isbn13 == "null") {
            isbn = isbn10;
        } else if(isbn10 == "null" && isbn13 == "null") {
            isbn = "-";
        } else {
            isbn = isbn10 + " / " + isbn13;
        }
        return isbn;
    }

    public String getAvailability() {
        try {
            return (Integer.parseInt(stockQuantity) > 0) ? "In Stock" : "Not Available";
        } catch (Exception e) {
            return "-";
        }
    }
}
