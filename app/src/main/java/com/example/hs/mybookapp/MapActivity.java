package com.example.hs.mybookapp;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;

import com.example.hs.mybookapp.map.Floor;
import com.example.hs.mybookapp.map.Shelf;

import java.util.ArrayList;
import java.util.LinkedList;


public class MapActivity extends Activity {

    private static final String TAG = "MapActivity";
    private static final String TAG_sourceShelf = "start";
    private static final String TAG_destinationShelf = "destination";

    private DrawMap map_image;
    private static int source_X, source_Y;
    private static int destination_X, destination_Y;
    private static Point source_point;
    private static Floor floor;
    private static LinkedList<Point> path = new LinkedList<>();

    private ArrayList<Point> pointList = new ArrayList<Point>(){
        {
            add(new Point(Shelf.A_X, Shelf.A_Y)); // A      0
            add(new Point(Shelf.B_X, Shelf.B_Y)); // B      1
            add(new Point(Shelf.C_X, Shelf.C_Y)); // C      2
            add(new Point(Shelf.D_X, Shelf.D_Y)); // D      3
            add(new Point(Shelf.E_X, Shelf.E_Y)); // E      4
            add(new Point(Shelf.F_X, Shelf.F_Y)); // F      5
            add(new Point(Shelf.G_X, Shelf.G_Y)); // G      6
            add(new Point(Shelf.H_X, Shelf.H_Y)); // H      7
            add(new Point(Shelf.I_X, Shelf.I_Y)); // I      8
            add(new Point(Shelf.J_X, Shelf.J_Y)); // J      9

            // Intersection points
            add(new Point(185, 200)); // Door   10
            add(new Point(185, 355)); // A      11
            add(new Point(185, 465)); // trunk  12
            add(new Point(185, 650)); // B-D    13
            add(new Point(185, 955)); // C-E    14
            add(new Point(540, 355)); // H      15
            add(new Point(540, 465)); // trunk  16
            add(new Point(540, 650)); // F-I    17
            add(new Point(540, 955)); // G-J    18
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        map_image = (DrawMap) findViewById(R.id.map_image);

        // Get source and destination shelf
        Bundle extras = getIntent().getExtras();

        if(extras != null) {
            source_X = Shelf.getX(extras.getString(TAG_sourceShelf).charAt(0));
            source_Y = Shelf.getY(extras.getString(TAG_sourceShelf).charAt(0));
            destination_X = Shelf.getX(extras.getString(TAG_destinationShelf).charAt(0));
            destination_Y = Shelf.getY(extras.getString(TAG_destinationShelf).charAt(0));
        }

        source_point = new Point(source_X, source_Y);
        initFloor();
    }

    @Override
    protected void onResume() {
        super.onResume();

        floor.addNode(source_point);
        floor.addLane("", pointList.size(), getNearestIndexFrom(pointList, source_point));

        Point destination_Point = new Point(destination_X, destination_Y);
        Point nearest_Point = getNearestFrom(pointList, destination_Point);
        path = floor.getRoute(pointList.size(), getNearestIndexFrom(pointList, nearest_Point));

        map_image.pathToDraw(path, destination_Point);
        map_image.invalidate();

        // Logging purpose
        for(int i = 0; i < path.size() - 1; i++) {
            Log.d(TAG + "====", "From X : " + path.get(i).x + " From Y : " + path.get(i).y);
            Log.d(TAG + "====", "To X : " + path.get(i+1).x + " To Y : " + path.get(i+1).y);
        }
    }

    private Point getNearestFrom(ArrayList<Point> sourceList,Point source){
        Point nearest;
        int minDistance;

        minDistance = calculateDuration(sourceList.get(0),source);
        nearest = sourceList.get(0);
        for(Point p: sourceList){
            if(!source.equals(p)){
                if(calculateDuration(source,p) < minDistance){
                    nearest = p;
                    minDistance = calculateDuration(source,p);
                }
            }
        }
        Log.d(TAG,"Nearest Point : " + nearest + "\tDistance : " + minDistance);
        return nearest;
    }

    private int getNearestIndexFrom(ArrayList<Point> sourceList,Point source){
        int nearestIndex=0;
        int minDistance;

        minDistance = calculateDuration(sourceList.get(0), source);
        for(int i = 0 ; i < sourceList.size() ; i++){
            Log.d(TAG,"in loop : "+ sourceList.get(i) + "\t" + calculateDuration(source,sourceList.get(i)) + "\tmin : " + minDistance);
            if(calculateDuration(source,sourceList.get(i)) < minDistance){
                nearestIndex = i;
                minDistance = calculateDuration(source,sourceList.get(i));
            }
        }

        Log.d(TAG,"Source : " + source);
        Log.d(TAG, "Nearest Point : " + sourceList.get(nearestIndex) + "\tDistance : " + minDistance);
        return nearestIndex;
    }

    private int calculateDuration(Point source,Point dest){
        return (int)Math.sqrt(Math.pow(source.x - dest.x, 2) + Math.pow(source.y - dest.y,2));
    }

    private void initFloor(){
        floor = new Floor();

        for(Point node : pointList) {
            floor.addNode(node);
        }

        floor.addLane("Door", 10, 11);
        floor.addLane("A", 0, 11);
        floor.addLane("B", 1, 13);
        floor.addLane("C", 2, 14);
        floor.addLane("D", 3, 13);
        floor.addLane("E", 4, 14);
        floor.addLane("F", 5, 17);
        floor.addLane("G", 6, 18);
        floor.addLane("H", 7, 15);
        floor.addLane("I", 8, 17);
        floor.addLane("J", 9, 18);
        floor.addLane("L-R trunk", 12, 16);
        floor.addLane("trunk", 11, 12);
        floor.addLane("trunk", 12, 13);
        floor.addLane("trunk", 13, 14);
        floor.addLane("trunk", 15, 16);
        floor.addLane("trunk", 16, 17);
        floor.addLane("trunk", 17, 18);
    }
}
