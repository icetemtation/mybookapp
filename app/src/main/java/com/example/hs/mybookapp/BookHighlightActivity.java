package com.example.hs.mybookapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import com.devsmart.android.ui.HorizontalListView;

public class BookHighlightActivity extends ActionBarActivity {

    // --- Custom TAGs
    private static final String TAG_isbn = "isbn";
    private static final String TAG_availability = "availability";

    ArrayList<Book> bookList_newArrivals, bookList_bestSellers, bookList_topSearchers;
    private static final int SUCCESS = 1;

    private JSONArray bookArray_newArrivals, bookArray_bestSellers, bookArray_topSearchers = null;
    private static HorizontalListView scroll_newArrivals, scroll_bestSellers, scroll_topSearchers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_highlight);

        scroll_newArrivals = (HorizontalListView) findViewById(R.id.scroll_newArrivals);
        scroll_bestSellers = (HorizontalListView) findViewById(R.id.scroll_bestSellers);
        scroll_topSearchers = (HorizontalListView) findViewById(R.id.scroll_topSearchers);

        new NewArrivalsTask().execute();
        new BestSellersTask().execute();
        new TopSearchersTask().execute();
    }

    private class NewArrivalsTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            bookList_newArrivals = new ArrayList<>();
            JSONParser jParser = new JSONParser();
            Log.d("NewArrivalsTask","Do In Background");
            try {
                // Sending Keyword to server
                List<NameValuePair> kv = new ArrayList<>();

                // Make HTTP POST request
                JSONObject json = jParser.makeHttpRequest(Constant.URL_getNewArrivals, "POST", kv);

                // Getting result from server
                int status = json.getInt(Constant.status);

                if (status == SUCCESS) {
                    Log.d("NewArrivalsTask","JSON SUCCESS");
                    bookArray_newArrivals = json.getJSONArray(Constant.books);

                    for (int i = 0; i < bookArray_newArrivals.length(); i++) {
                        JSONObject bookObject = bookArray_newArrivals.getJSONObject(i);
                        Book temp_book = new Book();
                        temp_book.setId(bookObject.getString(Constant.id));
                        temp_book.setIsbn10(bookObject.getString(Constant.isbn10));
                        temp_book.setIsbn13(bookObject.getString(Constant.isbn13));
                        temp_book.setTitle(bookObject.getString(Constant.title));
                        temp_book.setPrice(bookObject.getString(Constant.price));
                        temp_book.setGenre(bookObject.getString(Constant.genre));
                        temp_book.setAuthors(bookObject.getString(Constant.authors));
                        temp_book.setPublisher(bookObject.getString(Constant.publisher));
                        temp_book.setPublishedDate(bookObject.getString(Constant.publishedDate));
                        temp_book.setDescriptionShort(bookObject.getString(Constant.descShort));
                        temp_book.setDescriptionLong(bookObject.getString(Constant.descLong));
                        temp_book.setThumbnail(bookObject.getString(Constant.thumbnail));
                        temp_book.setPreview(bookObject.getString(Constant.preview));
                        temp_book.setStockQuantity(bookObject.getString(Constant.stockQuantity));
                        temp_book.setLocation(bookObject.getString(Constant.location));
                        bookList_newArrivals.add(temp_book);
                    }
                }
                return true;
            }
            catch (Exception e) {
                Log.d("NewArrivalsTask",e.getMessage());
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            if(aBoolean) {
                ListAdapter adapter = new HighlightAdapter(getApplicationContext(), R.layout.list_book_single, bookList_newArrivals);
                scroll_newArrivals.setAdapter(adapter);
                scroll_newArrivals.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent i = new Intent(getBaseContext(), BookInformationActivity.class);
                        i.putExtra(Constant.id, bookList_newArrivals.get(position).getId());
                        i.putExtra(Constant.isbn10, bookList_newArrivals.get(position).getIsbn10());
                        i.putExtra(Constant.isbn13, bookList_newArrivals.get(position).getIsbn13());
                        i.putExtra(Constant.title, bookList_newArrivals.get(position).getTitle());
                        i.putExtra(Constant.price, bookList_newArrivals.get(position).getPrice());
                        i.putExtra(Constant.genre, bookList_newArrivals.get(position).getGenre());
                        i.putExtra(Constant.authors, bookList_newArrivals.get(position).getAuthors());
                        i.putExtra(Constant.publisher, bookList_newArrivals.get(position).getPublisher());
                        i.putExtra(Constant.publishedDate, bookList_newArrivals.get(position).getPublishedDate());
                        i.putExtra(Constant.descShort, bookList_newArrivals.get(position).getDescriptionShort());
                        i.putExtra(Constant.descLong, bookList_newArrivals.get(position).getDescriptionLong());
                        i.putExtra(Constant.thumbnail, bookList_newArrivals.get(position).getThumbnail());
                        i.putExtra(Constant.preview, bookList_newArrivals.get(position).getPreview());
                        i.putExtra(Constant.stockQuantity, bookList_newArrivals.get(position).getStockQuantity());
                        i.putExtra(Constant.location, bookList_newArrivals.get(position).getLocation());
                        i.putExtra(TAG_isbn, bookList_newArrivals.get(position).getIsbn());
                        i.putExtra(TAG_availability, bookList_newArrivals.get(position).getAvailability());
                        i.putExtra(Constant.TAG_mode, "NewArrivalsTask");
                        startActivity(i);
                    }
                });
            }
        }
    }

    private class BestSellersTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            bookList_bestSellers = new ArrayList<>();
            JSONParser jParser = new JSONParser();
            Log.d("BestSellersTask","Do In Background");
            try {
                // Sending Keyword to server
                List<NameValuePair> kv = new ArrayList<>();

                // Make HTTP POST request
                JSONObject json = jParser.makeHttpRequest(Constant.URL_getBestSellers, "POST", kv);

                // Getting result from server
                int status = json.getInt(Constant.status);

                if (status == SUCCESS) {
                    Log.d("BestSellersTask","JSON SUCCESS");
                    bookArray_bestSellers = json.getJSONArray(Constant.books);

                    for (int i = 0; i < bookArray_bestSellers.length(); i++) {
                        JSONObject bookObject = bookArray_bestSellers.getJSONObject(i);
                        Book temp_book = new Book();
                        temp_book.setId(bookObject.getString(Constant.id));
                        temp_book.setIsbn10(bookObject.getString(Constant.isbn10));
                        temp_book.setIsbn13(bookObject.getString(Constant.isbn13));
                        temp_book.setTitle(bookObject.getString(Constant.title));
                        temp_book.setPrice(bookObject.getString(Constant.price));
                        temp_book.setGenre(bookObject.getString(Constant.genre));
                        temp_book.setAuthors(bookObject.getString(Constant.authors));
                        temp_book.setPublisher(bookObject.getString(Constant.publisher));
                        temp_book.setPublishedDate(bookObject.getString(Constant.publishedDate));
                        temp_book.setDescriptionShort(bookObject.getString(Constant.descShort));
                        temp_book.setDescriptionLong(bookObject.getString(Constant.descLong));
                        temp_book.setThumbnail(bookObject.getString(Constant.thumbnail));
                        temp_book.setPreview(bookObject.getString(Constant.preview));
                        temp_book.setStockQuantity(bookObject.getString(Constant.stockQuantity));
                        temp_book.setLocation(bookObject.getString(Constant.location));
                        bookList_bestSellers.add(temp_book);
                    }
                }
                return true;
            }
            catch (Exception e) {
                Log.d("BestSellersTask",e.getMessage());
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            if(aBoolean) {
                ListAdapter adapter = new HighlightAdapter(getApplicationContext(), R.layout.list_book_single, bookList_bestSellers);
                scroll_bestSellers.setAdapter(adapter);
                scroll_bestSellers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent i = new Intent(getBaseContext(), BookInformationActivity.class);
                        i.putExtra(Constant.id, bookList_bestSellers.get(position).getId());
                        i.putExtra(Constant.isbn10, bookList_bestSellers.get(position).getIsbn10());
                        i.putExtra(Constant.isbn13, bookList_bestSellers.get(position).getIsbn13());
                        i.putExtra(Constant.title, bookList_bestSellers.get(position).getTitle());
                        i.putExtra(Constant.price, bookList_bestSellers.get(position).getPrice());
                        i.putExtra(Constant.genre, bookList_bestSellers.get(position).getGenre());
                        i.putExtra(Constant.authors, bookList_bestSellers.get(position).getAuthors());
                        i.putExtra(Constant.publisher, bookList_bestSellers.get(position).getPublisher());
                        i.putExtra(Constant.publishedDate, bookList_bestSellers.get(position).getPublishedDate());
                        i.putExtra(Constant.descShort, bookList_bestSellers.get(position).getDescriptionShort());
                        i.putExtra(Constant.descLong, bookList_bestSellers.get(position).getDescriptionLong());
                        i.putExtra(Constant.thumbnail, bookList_bestSellers.get(position).getThumbnail());
                        i.putExtra(Constant.preview, bookList_bestSellers.get(position).getPreview());
                        i.putExtra(Constant.stockQuantity, bookList_bestSellers.get(position).getStockQuantity());
                        i.putExtra(Constant.location, bookList_bestSellers.get(position).getLocation());
                        i.putExtra(TAG_isbn, bookList_bestSellers.get(position).getIsbn());
                        i.putExtra(TAG_availability, bookList_bestSellers.get(position).getAvailability());
                        i.putExtra(Constant.TAG_mode, "BestSellersTask");
                        startActivity(i);
                    }
                });
            }
        }
    }

    private class TopSearchersTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            bookList_topSearchers = new ArrayList<>();
            JSONParser jParser = new JSONParser();
            Log.d("TopSearchersTask","Do In Background");
            try {
                // Sending Keyword to server
                List<NameValuePair> kv = new ArrayList<>();

                // Make HTTP POST request
                JSONObject json = jParser.makeHttpRequest(Constant.URL_getTopSearchers, "POST", kv);

                // Getting result from server
                int status = json.getInt(Constant.status);

                if (status == SUCCESS) {
                    Log.d("TopSearchersTask","JSON SUCCESS");
                    bookArray_topSearchers = json.getJSONArray(Constant.books);

                    for (int i = 0; i < bookArray_topSearchers.length(); i++) {
                        JSONObject bookObject = bookArray_topSearchers.getJSONObject(i);
                        Book temp_book = new Book();
                        temp_book.setId(bookObject.getString(Constant.id));
                        temp_book.setIsbn10(bookObject.getString(Constant.isbn10));
                        temp_book.setIsbn13(bookObject.getString(Constant.isbn13));
                        temp_book.setTitle(bookObject.getString(Constant.title));
                        temp_book.setPrice(bookObject.getString(Constant.price));
                        temp_book.setGenre(bookObject.getString(Constant.genre));
                        temp_book.setAuthors(bookObject.getString(Constant.authors));
                        temp_book.setPublisher(bookObject.getString(Constant.publisher));
                        temp_book.setPublishedDate(bookObject.getString(Constant.publishedDate));
                        temp_book.setDescriptionShort(bookObject.getString(Constant.descShort));
                        temp_book.setDescriptionLong(bookObject.getString(Constant.descLong));
                        temp_book.setThumbnail(bookObject.getString(Constant.thumbnail));
                        temp_book.setPreview(bookObject.getString(Constant.preview));
                        temp_book.setStockQuantity(bookObject.getString(Constant.stockQuantity));
                        temp_book.setLocation(bookObject.getString(Constant.location));
                        bookList_topSearchers.add(temp_book);
                    }
                }
                return true;
            }
            catch (Exception e) {
                Log.d("TopSearchersTask",e.getMessage());
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            Log.d("TopSearchersTask","onPostExecute");
            if(aBoolean) {
                ListAdapter adapter = new HighlightAdapter(getApplicationContext(), R.layout.list_book_single, bookList_topSearchers);
                scroll_topSearchers.setAdapter(adapter);
                scroll_topSearchers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent i = new Intent(getBaseContext(), BookInformationActivity.class);
                        i.putExtra(Constant.id, bookList_topSearchers.get(position).getId());
                        i.putExtra(Constant.isbn10, bookList_topSearchers.get(position).getIsbn10());
                        i.putExtra(Constant.isbn13, bookList_topSearchers.get(position).getIsbn13());
                        i.putExtra(Constant.title, bookList_topSearchers.get(position).getTitle());
                        i.putExtra(Constant.price, bookList_topSearchers.get(position).getPrice());
                        i.putExtra(Constant.genre, bookList_topSearchers.get(position).getGenre());
                        i.putExtra(Constant.authors, bookList_topSearchers.get(position).getAuthors());
                        i.putExtra(Constant.publisher, bookList_topSearchers.get(position).getPublisher());
                        i.putExtra(Constant.publishedDate, bookList_topSearchers.get(position).getPublishedDate());
                        i.putExtra(Constant.descShort, bookList_topSearchers.get(position).getDescriptionShort());
                        i.putExtra(Constant.descLong, bookList_topSearchers.get(position).getDescriptionLong());
                        i.putExtra(Constant.thumbnail, bookList_topSearchers.get(position).getThumbnail());
                        i.putExtra(Constant.preview, bookList_topSearchers.get(position).getPreview());
                        i.putExtra(Constant.stockQuantity, bookList_topSearchers.get(position).getStockQuantity());
                        i.putExtra(Constant.location, bookList_topSearchers.get(position).getLocation());
                        i.putExtra(TAG_isbn, bookList_topSearchers.get(position).getIsbn());
                        i.putExtra(TAG_availability, bookList_topSearchers.get(position).getAvailability());
                        i.putExtra(Constant.TAG_mode, "TopSearchersTask");
                        startActivity(i);
                    }
                });
            }
        }
    }

}
