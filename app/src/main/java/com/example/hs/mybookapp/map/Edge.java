package com.example.hs.mybookapp.map;

import android.graphics.Point;

public class Edge {
    private final String id;
    private final Point source;
    private final Point destination;
    private final int weight;

    public Edge(String id, Point source, Point destination, int weight) {
        this.id = id;
        this.source = source;
        this.destination = destination;
        this.weight = weight;
    }

    public String getId() { return id; }

    public Point getSource() { return source; }

    public Point getDestination() { return destination; }

    public int getWeight() {  return weight; }


    @Override
    public String toString(){
        return source+" "+destination;
    }
}
