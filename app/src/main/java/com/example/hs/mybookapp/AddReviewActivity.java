package com.example.hs.mybookapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class AddReviewActivity extends ActionBarActivity implements View.OnClickListener {

    // --- Tags
    private static final String TAG_id = "id";
    private static final String TAG_title = "title";
    private static final int SUCCESS = 2;
    // --- Views
    private static TextView v_bookTitle, v_name, v_comments;
    private static RatingBar r_ratingBar;
    private static Button b_cancel, b_submit;
    private static ProgressDialog p_dialog;
    // --- Variables
    private static Bundle bundle;
    private static String book_id, book_title;

    JSONParser jsonParser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get Book ID from previous intent
        bundle = getIntent().getExtras();

        if(bundle != null) {
            book_id = bundle.getString(TAG_id);
            book_title = bundle.getString(TAG_title);
        }
        setContentView(R.layout.activity_add_review);

        // Instantiate JSONParser
        jsonParser = new JSONParser();

        // Initialize variables
        v_bookTitle = (TextView) findViewById(R.id.addReview_bookTitle);
        v_name = (TextView) findViewById(R.id.addReview_name);
        v_comments = (TextView) findViewById(R.id.addReview_comments);
        r_ratingBar = (RatingBar) findViewById(R.id.addReview_ratingBar);
        b_cancel = (Button) findViewById(R.id.addReview_cancel);
        b_submit = (Button) findViewById(R.id.addReview_submit);

        // Set rating bar
        r_ratingBar.setRating(0);

        // Set onClickListerner to buttons
        b_cancel.setOnClickListener(this);
        b_submit.setOnClickListener(this);

        v_bookTitle.setText(book_title);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.addReview_cancel:
                finish();
                break;
            case R.id.addReview_submit:
                submit();
                break;
            default:
                break;
        }
    }

    public void submit() {
        if(validateField()) {
            new PostReview().execute();
        }
    }

    private boolean validateField() {
        if(r_ratingBar.getRating() <= 0) {
            displayAlertDialog(R.string.addReview_dialog_error_rating_title, R.string.addReview_dialog_error_rating_description);
        } else {
            if(v_comments.getText().toString().trim().length() == 0) {
                displayAlertDialog(R.string.addReview_dialog_error_comments_title, R.string.addReview_dialog_error_comments_description);
            } else {
                if(v_name.getText().toString().trim().length() == 0) {
                    v_name.setText("Anonymous");
                }
                return true;
            }
        }
        return false;
    }

    private void displayAlertDialog(int title, int description) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title)
                .setMessage(description)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    class PostReview extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            p_dialog = new ProgressDialog(AddReviewActivity.this);
            p_dialog.setMessage("Posting Review...");
            p_dialog.setIndeterminate(false);
            p_dialog.setCancelable(true);
            p_dialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            String review_name = v_name.getText().toString();
            String review_rating = r_ratingBar.getRating() + "";
            String review_comments = v_comments.getText().toString();

            try {
                // Building Key-value (kv) pairs parameters
                List<NameValuePair> kv = new ArrayList<>();
                kv.add(new BasicNameValuePair(Constant.Review.bookID, book_id));
                kv.add(new BasicNameValuePair(Constant.Review.name, review_name));
                kv.add(new BasicNameValuePair(Constant.Review.rating, review_rating));
                kv.add(new BasicNameValuePair(Constant.Review.comments, review_comments));

                // Make HTTP POST request to the server
                JSONObject json = jsonParser.makeHttpRequest(Constant.URL_addReview, "POST", kv);

                // Get returned status by the server
                int status = json.getInt(Constant.status);

                if (status == SUCCESS) {
                    return true;
                }

            } catch (Exception e) {
                Log.e("Error", e.getMessage());
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean status) {
            super.onPostExecute(status);
            p_dialog.dismiss();

            if(status) {
                Log.d("POST_REVIEW", "SUCCESS");
                Toast.makeText(AddReviewActivity.this, "[BookID : " + book_id + "] Review posted successfully", Toast.LENGTH_LONG).show();
                finish();
            } else {
                Log.d("POST_REVIEW", "FAILED");
                displayAlertDialog(R.string.addReview_dialog_error_server_title, R.string.addReview_dialog_error_server_description);
            }
        }
    }
}
