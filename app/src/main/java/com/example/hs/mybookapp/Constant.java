package com.example.hs.mybookapp;

public class Constant {
    // Book
    public static final String status = "status";
    public static final String totalMatched = "totalMatched";
    public static final String books = "books";
    public static final String id = "id";
    public static final String isbn10 = "isbn10";
    public static final String isbn13 = "isbn13";
    public static final String title = "title";
    public static final String price = "price";
    public static final String genre = "genre";
    public static final String authors = "authors";
    public static final String publisher = "publisher";
    public static final String publishedDate = "publishedDate";
    public static final String descShort = "descriptionShort";
    public static final String descLong = "descriptionLong";
    public static final String thumbnail = "thumbnail";
    public static final String preview = "preview";
    public static final String stockQuantity = "stockQuantity";
    public static final String location = "location";

    public static class Review {
        public static final String bookID = "book_id";
        public static final String reviews = "reviews";
        public static final String reviewId = "review_id";
        public static final String title = "title";
        public static final String rating = "review_rating";
        public static final String name = "review_name";
        public static final String comments = "review_comments";
        public static final String dateTime = "review_dateTime";
    }

    public static class Rating {
        public static final String rating = "rating";
        public static final String totalReviews = "totalReviews";
        public static final String averageRating = "averageRating";
    }

    public static class Preview {
        public static final String url = "webAddress";
    }

    public static final String URL_addReview = "http://mybookapp.comuf.com/addReview.php";
    public static final String URL_getAverageRating = "http://mybookapp.comuf.com/getAverageRating.php";
    public static final String URL_getReview = "http://mybookapp.comuf.com/getReview.php";
    public static final String URL_getBookListByKeywordOrISBN = "http://mybookapp.comuf.com/getBookListByKeywordOrISBN.php";
    public static final String URL_getBookListByShelf = "http://mybookapp.comuf.com/getBookListByShelf.php";
    public static final String URL_getNewArrivals = "http://mybookapp.comuf.com/getNewArrivals.php";
    public static final String URL_getBestSellers = "http://mybookapp.comuf.com/getBestSellers.php";
    public static final String URL_getTopSearchers = "http://mybookapp.comuf.com/getTopSearchers.php";

    public static final String TAG_mode = "mybookapp.mode";
    public static final String TAG_searchInput = "mybookapp.searchInput";

    public static final String MODE_details = "mode_details";
    public static final String MODE_reviews = "mode_reviews";
    public static final String MODE_shelfRecommendations = "mode_shelfRecommendations";

    public static class POST {
        public static final String keyword = "keyword";
        public static final String shelf = "shelf";
    }

}
